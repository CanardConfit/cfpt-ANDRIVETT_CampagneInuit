﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace P_CampagneDePecheChezLesInuits
{
    public class InuitCampagneModel
    {
        private static Random _random = new Random();
        
        private List<FisherMan> _fishermans;
        private List<Rule> _rules;
        private List<string> _names;
        private Dictionary<RuleType, int> _calculRules;

        public Dictionary<RuleType, int> CalculRules => _calculRules;

        public InuitCampagneModel()
        {
            _calculRules = new Dictionary<RuleType, int>();
            
            _names = new List<string>();
            _names.AddRange(new []
            {
                "Tom",
                "Arthur",
                "Jack",
                "Dimitri",
                "Thibault",
                "Julian",
                "Juliano",
                "Yann",
                "Dallas",
                "Fabio",
                "Aliya",
                "Michi",
                "Ludovic",
                "Eric",
                "Dan"
            });
            _rules = new List<Rule>();
            _rules.AddRange(new []
            {
                new Rule(RuleType.NumberBearHole, new []
                {
                    0,
                    0,
                    2,
                    0,
                    4,
                    0
                }),
                new Rule(RuleType.NumberFishHole, new []
                {
                    6,
                    0,
                    4,
                    0,
                    2,
                    0
                }),
                new Rule(RuleType.NumberHoleIce, new []
                {
                    1,
                    0,
                    1,
                    0,
                    1,
                    0
                    
                })
            });
            
            Reset();
            
            foreach (Rule rule in _rules) _calculRules.Add(rule.RuleType, 0);
        }

        private void Reset()
        {
            foreach (RuleType key in _calculRules.Keys.ToList()) _calculRules[key] = 0;

            _fishermans = new List<FisherMan>();
        }
        
        public bool GetFisherMan(int idFisherMan, out FisherMan fisherman)
        {
            if (_fishermans.Count <= idFisherMan)
            {
                fisherman = null;
                return false;
            }

            fisherman = _fishermans[idFisherMan];
            return true;

        }

        public void RollDices(int fisherManNumber)
        {
            Reset();
            for (int idFisher = 0; idFisher <= fisherManNumber; idFisher++)
            {
                string name = _names[_random.Next(_names.Count)];
                int diceNumber = _random.Next(1, 7);
                foreach (Rule rule in _rules) _calculRules[rule.RuleType] += rule.Values[diceNumber - 1];
                _fishermans.Add(new FisherMan(name, diceNumber));
            }
        }
    }
}