﻿namespace P_CampagneDePecheChezLesInuits
{
    public class Rule
    {
        private readonly RuleType _ruleType;
        private readonly int[] _values;

        public RuleType RuleType => _ruleType;

        public int[] Values => _values;

        public Rule(RuleType ruleType, int[] values)
        {
            _ruleType = ruleType;
            _values = values;
        }
    }
}