﻿namespace P_CampagneDePecheChezLesInuits
{
    public class FisherMan
    {
        private string _name;
        private int _diceNumber;

        public string Name => _name;

        public int DiceNumber => _diceNumber;

        public FisherMan(string name, int diceNumber)
        {
            _name = name;
            _diceNumber = diceNumber;
        }
    }
}