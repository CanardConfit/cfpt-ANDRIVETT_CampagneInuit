﻿using System;
using System.Windows.Forms;

namespace P_CampagneDePecheChezLesInuits
{
  static class Program
  {
    /// <summary>
    /// Point d'entrée principal de l'application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new frmCampagnePecheInuit()); 
    }
  }
}
