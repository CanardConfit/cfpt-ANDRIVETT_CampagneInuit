﻿namespace P_CampagneDePecheChezLesInuits
{
  partial class frmCampagnePecheInuit
  {
    /// <summary>
    /// Variable nécessaire au concepteur.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Nettoyage des ressources utilisées.
    /// </summary>
    /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Code généré par le Concepteur Windows Form

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCampagnePecheInuit));
      this.gbxDescription = new System.Windows.Forms.GroupBox();
      this.lblDescription = new System.Windows.Forms.Label();
      this.gbxTirage = new System.Windows.Forms.GroupBox();
      this.pbxDe1 = new System.Windows.Forms.PictureBox();
      this.pbxDe2 = new System.Windows.Forms.PictureBox();
      this.pbxDe3 = new System.Windows.Forms.PictureBox();
      this.pbxDe4 = new System.Windows.Forms.PictureBox();
      this.pbxDe5 = new System.Windows.Forms.PictureBox();
      this.pbxDe6 = new System.Windows.Forms.PictureBox();
      this.gbxGroupePecheur = new System.Windows.Forms.GroupBox();
      this.lblNbPecheur = new System.Windows.Forms.Label();
      this.cbxNombrePecheur = new System.Windows.Forms.ComboBox();
      this.gbxPrevisions = new System.Windows.Forms.GroupBox();
      this.lblPrevisions = new System.Windows.Forms.Label();
      this.gbxPseudonymes = new System.Windows.Forms.GroupBox();
      this.lblPseudonymesPecheurs = new System.Windows.Forms.Label();
      this.lblDeDuPecheur6 = new System.Windows.Forms.Label();
      this.lblDeDuPecheur5 = new System.Windows.Forms.Label();
      this.lblDeDuPecheur4 = new System.Windows.Forms.Label();
      this.lblDeDuPecheur3 = new System.Windows.Forms.Label();
      this.lblDeDuPecheur2 = new System.Windows.Forms.Label();
      this.lblDeDuPecheur1 = new System.Windows.Forms.Label();
      this.gbxDescription.SuspendLayout();
      this.gbxTirage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe6)).BeginInit();
      this.gbxGroupePecheur.SuspendLayout();
      this.gbxPrevisions.SuspendLayout();
      this.gbxPseudonymes.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxDescription
      // 
      this.gbxDescription.Controls.Add(this.lblDescription);
      this.gbxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.gbxDescription.Location = new System.Drawing.Point(25, 17);
      this.gbxDescription.Margin = new System.Windows.Forms.Padding(4);
      this.gbxDescription.Name = "gbxDescription";
      this.gbxDescription.Padding = new System.Windows.Forms.Padding(4);
      this.gbxDescription.Size = new System.Drawing.Size(925, 175);
      this.gbxDescription.TabIndex = 0;
      this.gbxDescription.TabStop = false;
      this.gbxDescription.Text = " Description ";
      // 
      // lblDescription
      // 
      this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDescription.Location = new System.Drawing.Point(27, 21);
      this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDescription.Name = "lblDescription";
      this.lblDescription.Size = new System.Drawing.Size(820, 139);
      this.lblDescription.TabIndex = 0;
      this.lblDescription.Text = resources.GetString("lblDescription.Text");
      // 
      // gbxTirage
      // 
      this.gbxTirage.Controls.Add(this.pbxDe1);
      this.gbxTirage.Controls.Add(this.pbxDe2);
      this.gbxTirage.Controls.Add(this.pbxDe3);
      this.gbxTirage.Controls.Add(this.pbxDe4);
      this.gbxTirage.Controls.Add(this.pbxDe5);
      this.gbxTirage.Controls.Add(this.pbxDe6);
      this.gbxTirage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.gbxTirage.Location = new System.Drawing.Point(25, 218);
      this.gbxTirage.Margin = new System.Windows.Forms.Padding(4);
      this.gbxTirage.Name = "gbxTirage";
      this.gbxTirage.Padding = new System.Windows.Forms.Padding(4);
      this.gbxTirage.Size = new System.Drawing.Size(925, 130);
      this.gbxTirage.TabIndex = 1;
      this.gbxTirage.TabStop = false;
      this.gbxTirage.Text = " Tirage ";
      // 
      // pbxDe1
      // 
      this.pbxDe1.Image = global::P_CampagneDePecheChezLesInuits.Properties.Resources._1;
      this.pbxDe1.Location = new System.Drawing.Point(121, 20);
      this.pbxDe1.Margin = new System.Windows.Forms.Padding(4);
      this.pbxDe1.Name = "pbxDe1";
      this.pbxDe1.Size = new System.Drawing.Size(100, 92);
      this.pbxDe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbxDe1.TabIndex = 2;
      this.pbxDe1.TabStop = false;
      // 
      // pbxDe2
      // 
      this.pbxDe2.Image = global::P_CampagneDePecheChezLesInuits.Properties.Resources._2;
      this.pbxDe2.Location = new System.Drawing.Point(251, 20);
      this.pbxDe2.Margin = new System.Windows.Forms.Padding(4);
      this.pbxDe2.Name = "pbxDe2";
      this.pbxDe2.Size = new System.Drawing.Size(100, 92);
      this.pbxDe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbxDe2.TabIndex = 3;
      this.pbxDe2.TabStop = false;
      // 
      // pbxDe3
      // 
      this.pbxDe3.Image = global::P_CampagneDePecheChezLesInuits.Properties.Resources._3;
      this.pbxDe3.Location = new System.Drawing.Point(380, 20);
      this.pbxDe3.Margin = new System.Windows.Forms.Padding(4);
      this.pbxDe3.Name = "pbxDe3";
      this.pbxDe3.Size = new System.Drawing.Size(100, 92);
      this.pbxDe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbxDe3.TabIndex = 4;
      this.pbxDe3.TabStop = false;
      // 
      // pbxDe4
      // 
      this.pbxDe4.Image = global::P_CampagneDePecheChezLesInuits.Properties.Resources._4;
      this.pbxDe4.Location = new System.Drawing.Point(509, 20);
      this.pbxDe4.Margin = new System.Windows.Forms.Padding(4);
      this.pbxDe4.Name = "pbxDe4";
      this.pbxDe4.Size = new System.Drawing.Size(100, 92);
      this.pbxDe4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbxDe4.TabIndex = 5;
      this.pbxDe4.TabStop = false;
      // 
      // pbxDe5
      // 
      this.pbxDe5.Image = global::P_CampagneDePecheChezLesInuits.Properties.Resources._5;
      this.pbxDe5.Location = new System.Drawing.Point(639, 20);
      this.pbxDe5.Margin = new System.Windows.Forms.Padding(4);
      this.pbxDe5.Name = "pbxDe5";
      this.pbxDe5.Size = new System.Drawing.Size(100, 92);
      this.pbxDe5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbxDe5.TabIndex = 6;
      this.pbxDe5.TabStop = false;
      // 
      // pbxDe6
      // 
      this.pbxDe6.Image = global::P_CampagneDePecheChezLesInuits.Properties.Resources._6;
      this.pbxDe6.Location = new System.Drawing.Point(768, 20);
      this.pbxDe6.Margin = new System.Windows.Forms.Padding(4);
      this.pbxDe6.Name = "pbxDe6";
      this.pbxDe6.Size = new System.Drawing.Size(100, 92);
      this.pbxDe6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbxDe6.TabIndex = 7;
      this.pbxDe6.TabStop = false;
      // 
      // gbxGroupePecheur
      // 
      this.gbxGroupePecheur.Controls.Add(this.lblNbPecheur);
      this.gbxGroupePecheur.Controls.Add(this.cbxNombrePecheur);
      this.gbxGroupePecheur.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.gbxGroupePecheur.Location = new System.Drawing.Point(25, 373);
      this.gbxGroupePecheur.Margin = new System.Windows.Forms.Padding(4);
      this.gbxGroupePecheur.Name = "gbxGroupePecheur";
      this.gbxGroupePecheur.Padding = new System.Windows.Forms.Padding(4);
      this.gbxGroupePecheur.Size = new System.Drawing.Size(352, 84);
      this.gbxGroupePecheur.TabIndex = 2;
      this.gbxGroupePecheur.TabStop = false;
      this.gbxGroupePecheur.Text = "Nombre de pêcheurs ";
      // 
      // lblNbPecheur
      // 
      this.lblNbPecheur.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblNbPecheur.Location = new System.Drawing.Point(211, 33);
      this.lblNbPecheur.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblNbPecheur.Name = "lblNbPecheur";
      this.lblNbPecheur.Size = new System.Drawing.Size(103, 20);
      this.lblNbPecheur.TabIndex = 15;
      this.lblNbPecheur.Text = "Pêcheur(s)";
      this.lblNbPecheur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cbxNombrePecheur
      // 
      this.cbxNombrePecheur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxNombrePecheur.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.cbxNombrePecheur.ForeColor = System.Drawing.Color.Blue;
      this.cbxNombrePecheur.FormattingEnabled = true;
      this.cbxNombrePecheur.Items.AddRange(new object[] {"1", "2", "3", "4", "5", "6"});
      this.cbxNombrePecheur.Location = new System.Drawing.Point(39, 27);
      this.cbxNombrePecheur.Margin = new System.Windows.Forms.Padding(4);
      this.cbxNombrePecheur.Name = "cbxNombrePecheur";
      this.cbxNombrePecheur.Size = new System.Drawing.Size(159, 32);
      this.cbxNombrePecheur.TabIndex = 1;
      this.cbxNombrePecheur.SelectedIndexChanged += new System.EventHandler(this.cbxNombrePecheur_SelectedIndexChanged);
      // 
      // gbxPrevisions
      // 
      this.gbxPrevisions.Controls.Add(this.lblPrevisions);
      this.gbxPrevisions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.gbxPrevisions.Location = new System.Drawing.Point(405, 373);
      this.gbxPrevisions.Margin = new System.Windows.Forms.Padding(4);
      this.gbxPrevisions.Name = "gbxPrevisions";
      this.gbxPrevisions.Padding = new System.Windows.Forms.Padding(4);
      this.gbxPrevisions.Size = new System.Drawing.Size(545, 274);
      this.gbxPrevisions.TabIndex = 3;
      this.gbxPrevisions.TabStop = false;
      this.gbxPrevisions.Text = " Prévisions ";
      // 
      // lblPrevisions
      // 
      this.lblPrevisions.Font = new System.Drawing.Font("Arial Narrow", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblPrevisions.ForeColor = System.Drawing.Color.Blue;
      this.lblPrevisions.Location = new System.Drawing.Point(27, 18);
      this.lblPrevisions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblPrevisions.Name = "lblPrevisions";
      this.lblPrevisions.Size = new System.Drawing.Size(500, 236);
      this.lblPrevisions.TabIndex = 0;
      this.lblPrevisions.Text = "Affichage des prévisions...";
      this.lblPrevisions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gbxPseudonymes
      // 
      this.gbxPseudonymes.Controls.Add(this.lblPseudonymesPecheurs);
      this.gbxPseudonymes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.gbxPseudonymes.Location = new System.Drawing.Point(25, 475);
      this.gbxPseudonymes.Margin = new System.Windows.Forms.Padding(4);
      this.gbxPseudonymes.Name = "gbxPseudonymes";
      this.gbxPseudonymes.Padding = new System.Windows.Forms.Padding(4);
      this.gbxPseudonymes.Size = new System.Drawing.Size(351, 172);
      this.gbxPseudonymes.TabIndex = 4;
      this.gbxPseudonymes.TabStop = false;
      this.gbxPseudonymes.Text = " Pseudonymes des pêcheurs ";
      // 
      // lblPseudonymesPecheurs
      // 
      this.lblPseudonymesPecheurs.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblPseudonymesPecheurs.Location = new System.Drawing.Point(35, 20);
      this.lblPseudonymesPecheurs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblPseudonymesPecheurs.Name = "lblPseudonymesPecheurs";
      this.lblPseudonymesPecheurs.Size = new System.Drawing.Size(308, 145);
      this.lblPseudonymesPecheurs.TabIndex = 15;
      this.lblPseudonymesPecheurs.Text = "Pêcheur 1 : pseudo du pêcheur 1\r\nPêcheur 2 : pseudo du pêcheur 2\r\nPêcheur 3 : pse" + "udo du pêcheur 3\r\nPêcheur 4 : pseudo du pêcheur 4\r\nPêcheur 5 : pseudo du pêcheur" + " 5\r\nPêcheur 6 : pseudo du pêcheur 6";
      this.lblPseudonymesPecheurs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblDeDuPecheur6
      // 
      this.lblDeDuPecheur6.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDeDuPecheur6.Location = new System.Drawing.Point(792, 203);
      this.lblDeDuPecheur6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDeDuPecheur6.Name = "lblDeDuPecheur6";
      this.lblDeDuPecheur6.Size = new System.Drawing.Size(103, 20);
      this.lblDeDuPecheur6.TabIndex = 19;
      this.lblDeDuPecheur6.Text = "Dé du pêcheur 6";
      this.lblDeDuPecheur6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblDeDuPecheur5
      // 
      this.lblDeDuPecheur5.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDeDuPecheur5.Location = new System.Drawing.Point(663, 203);
      this.lblDeDuPecheur5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDeDuPecheur5.Name = "lblDeDuPecheur5";
      this.lblDeDuPecheur5.Size = new System.Drawing.Size(103, 20);
      this.lblDeDuPecheur5.TabIndex = 18;
      this.lblDeDuPecheur5.Text = "Dé du pêcheur 5";
      this.lblDeDuPecheur5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblDeDuPecheur4
      // 
      this.lblDeDuPecheur4.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDeDuPecheur4.Location = new System.Drawing.Point(533, 203);
      this.lblDeDuPecheur4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDeDuPecheur4.Name = "lblDeDuPecheur4";
      this.lblDeDuPecheur4.Size = new System.Drawing.Size(103, 20);
      this.lblDeDuPecheur4.TabIndex = 17;
      this.lblDeDuPecheur4.Text = "Dé du pêcheur 4";
      this.lblDeDuPecheur4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblDeDuPecheur3
      // 
      this.lblDeDuPecheur3.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDeDuPecheur3.Location = new System.Drawing.Point(404, 203);
      this.lblDeDuPecheur3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDeDuPecheur3.Name = "lblDeDuPecheur3";
      this.lblDeDuPecheur3.Size = new System.Drawing.Size(103, 20);
      this.lblDeDuPecheur3.TabIndex = 16;
      this.lblDeDuPecheur3.Text = "Dé du pêcheur 3";
      this.lblDeDuPecheur3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblDeDuPecheur2
      // 
      this.lblDeDuPecheur2.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDeDuPecheur2.Location = new System.Drawing.Point(275, 203);
      this.lblDeDuPecheur2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDeDuPecheur2.Name = "lblDeDuPecheur2";
      this.lblDeDuPecheur2.Size = new System.Drawing.Size(103, 20);
      this.lblDeDuPecheur2.TabIndex = 15;
      this.lblDeDuPecheur2.Text = "Dé du pêcheur 2";
      this.lblDeDuPecheur2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblDeDuPecheur1
      // 
      this.lblDeDuPecheur1.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
      this.lblDeDuPecheur1.Location = new System.Drawing.Point(145, 203);
      this.lblDeDuPecheur1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDeDuPecheur1.Name = "lblDeDuPecheur1";
      this.lblDeDuPecheur1.Size = new System.Drawing.Size(103, 20);
      this.lblDeDuPecheur1.TabIndex = 14;
      this.lblDeDuPecheur1.Text = "Dé du pêcheur 1";
      this.lblDeDuPecheur1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // frmCampagnePecheInuit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(973, 668);
      this.Controls.Add(this.lblDeDuPecheur6);
      this.Controls.Add(this.lblDeDuPecheur5);
      this.Controls.Add(this.lblDeDuPecheur4);
      this.Controls.Add(this.lblDeDuPecheur3);
      this.Controls.Add(this.lblDeDuPecheur2);
      this.Controls.Add(this.lblDeDuPecheur1);
      this.Controls.Add(this.gbxPseudonymes);
      this.Controls.Add(this.gbxPrevisions);
      this.Controls.Add(this.gbxGroupePecheur);
      this.Controls.Add(this.gbxTirage);
      this.Controls.Add(this.gbxDescription);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
      this.Margin = new System.Windows.Forms.Padding(4);
      this.MaximizeBox = false;
      this.Name = "frmCampagnePecheInuit";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Campagne de pêche chez les Inuits...";
      this.Load += new System.EventHandler(this.frmCampagnePecheInuit_Load);
      this.gbxDescription.ResumeLayout(false);
      this.gbxTirage.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe1)).EndInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe2)).EndInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe3)).EndInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe4)).EndInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe5)).EndInit();
      ((System.ComponentModel.ISupportInitialize) (this.pbxDe6)).EndInit();
      this.gbxGroupePecheur.ResumeLayout(false);
      this.gbxPrevisions.ResumeLayout(false);
      this.gbxPseudonymes.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    #endregion

    private System.Windows.Forms.GroupBox gbxDescription;
    private System.Windows.Forms.GroupBox gbxTirage;
    private System.Windows.Forms.GroupBox gbxGroupePecheur;
    private System.Windows.Forms.GroupBox gbxPrevisions;
    private System.Windows.Forms.Label lblDescription;
    private System.Windows.Forms.Label lblPrevisions;
    private System.Windows.Forms.PictureBox pbxDe6;
    private System.Windows.Forms.PictureBox pbxDe5;
    private System.Windows.Forms.PictureBox pbxDe4;
    private System.Windows.Forms.PictureBox pbxDe3;
    private System.Windows.Forms.PictureBox pbxDe2;
    private System.Windows.Forms.PictureBox pbxDe1;
    private System.Windows.Forms.ComboBox cbxNombrePecheur;
    private System.Windows.Forms.GroupBox gbxPseudonymes;
    private System.Windows.Forms.Label lblNbPecheur;
    private System.Windows.Forms.Label lblDeDuPecheur6;
    private System.Windows.Forms.Label lblDeDuPecheur5;
    private System.Windows.Forms.Label lblDeDuPecheur4;
    private System.Windows.Forms.Label lblDeDuPecheur3;
    private System.Windows.Forms.Label lblDeDuPecheur2;
    private System.Windows.Forms.Label lblDeDuPecheur1;
    private System.Windows.Forms.Label lblPseudonymesPecheurs;
  }
}

