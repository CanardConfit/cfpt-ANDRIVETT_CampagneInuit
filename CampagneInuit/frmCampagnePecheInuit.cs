﻿/*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Projet  : Campagne de pêche chez les Inuits
 * Desc.   : Vue (WinForm) de l'application : "Campagne de pêche chez les Inuits"
 * Auteur  : Alec BENEY (version initiale)
 * Version : 5.0
 * Date    : Septembre 2019
 * * * * * * * * * * * * * * * * * COMPLÉMENTS * * * * * * * * * * * * * * * * *  
 * Auteur  : Tom Andrivet
 * Classe  : I.FA-P3A
 * Version : 6.0
 * Date    : 18.03.2021
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using P_CampagneDePecheChezLesInuits.Properties;

namespace P_CampagneDePecheChezLesInuits
{
    public partial class frmCampagnePecheInuit : Form
    {
        private InuitCampagneModel _model;
        
        public frmCampagnePecheInuit() {
            InitializeComponent();
        }

        private void frmCampagnePecheInuit_Load(object sender, EventArgs e)
        {
            _model = new InuitCampagneModel();
            Print();
        }

        private void Print()
        {
            StringBuilder nomsPecheursBuilder = new StringBuilder();
            for (int idFisherMan = 1; idFisherMan <= 6; idFisherMan++)
            {
                PictureBox pbx = gbxTirage.Controls.Find($"pbxDe{idFisherMan}", false)[0] as PictureBox;

                if (_model.GetFisherMan(idFisherMan, out FisherMan fisherman))
                {
                    nomsPecheursBuilder.AppendLine($"Pecheur {idFisherMan} : {fisherman.Name}");
                    pbx.Image = (Image) Resources.ResourceManager.GetObject($"_{fisherman.DiceNumber}", Resources.Culture);
                }
                else
                    pbx.Image = null;
            }

            lblPseudonymesPecheurs.Text = nomsPecheursBuilder.ToString();
            
            lblPrevisions.Text = $"Nombre de trous à creuser dans la glace : {_model.CalculRules[RuleType.NumberHoleIce]}\r\n" +
                                 $"Nombre de poissons dans les trous : {_model.CalculRules[RuleType.NumberFishHole]}\r\n" +
                                 $"Nombre d'ours qui viendront rôder autour des trous : {_model.CalculRules[RuleType.NumberBearHole]}";
        }

        private void cbxNombrePecheur_SelectedIndexChanged(object sender, EventArgs e)
        {
            _model.RollDices(Convert.ToInt32(cbxNombrePecheur.SelectedItem));
            Print();
        }
    }
}
